﻿using System;
using System.Collections.Generic;
using System.Linq;
using Yidlo.Model;


namespace ServiceLayer
{
    public class CourseRepositoryStub : IYidloService<Course>
    {
        private static  readonly object syncObj =new object();

        private static CourseRepositoryStub _courseService;

        public CourseRepositoryStub()
        {
            PopulateTestData();
        }

        public static CourseRepositoryStub CourseService
        {
            get
            {
                if (_courseService != null) return _courseService;
                lock (syncObj)
                {
                    return _courseService ?? (_courseService = new CourseRepositoryStub());
                }
            }
        }

        private void PopulateTestData()
        {
            bool[] availability = { true, true, false, true, false, false, false };

            _courses.Add(new Course(1, "Картофель по-деревенски", "Картофель с поджаренным луком, " +
                    "с нарезаной зеленью и легким ароматом чеснока", 38, "Второе", availability));
        }

        private List<Course> _courses = new List<Course>();

        public ResponseMessage Add(Course course)
        {
            course.Id = (_courses.Count > 0 ? _courses.Last().Id + 1 : 0);
            _courses.Add(course);
                return new ResponseMessage();
        }

        public Course Get(int ID)
        {
            return _courses.FirstOrDefault(i=>i.Id == ID);
        }

        public Course Logining(string Login, string Password)
        {
            throw new NotImplementedException();
        }

        public List<Course> GetAll()
        {
            return _courses;
        }

        public ResponseMessage Update(Course entity)
        {
            int matchedID = _courses.FindIndex(x => x.Id == entity.Id);

            if (matchedID == -1)
                throw new ArgumentException("Course with this Id haven't benn found.");

            _courses[matchedID] = entity;

            return  new ResponseMessage();
        }

        public ResponseMessage Remove(int id)
        {
            try
            {
                _courses.RemoveAll(x => x.Id == id);
            }
            catch (Exception ex)
            {
                return new ResponseMessage() {Details = ex.Message};
            }

            return new ResponseMessage();
        }

        public int CoursesCount => _courses.Count;
    }
}