﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Yidlo.Model;

namespace ServiceLayer
{
    public class CourseService : IYidloService<Course>
    {
        private const string serviceUri = "http://10.10.3.97:8080";

        public ResponseMessage Add(Course course)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri(serviceUri);

            var request = new RestRequest(Method.POST);
            request.Resource = "courses/add";
            request.AddHeader("Content-type", "application/json");
            string json = JsonConvert.SerializeObject(course);

            request.AddBody(course);

            IRestResponse response = client.Execute(request);

            var test = response.Content;
            return new ResponseMessage();
        }

        public Course Get(int Id)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri(serviceUri);

            var request = new RestRequest();
            request.Resource = "courses/" + Id;

            IRestResponse response = client.Execute(request);

            return ServiceObject.ServiceEntity.TryParse<ServiceObject.Course>(response.Content);
        }

        public Course Logining(string Login, string Password)
        {
            throw new NotImplementedException();
        }

        public List<Course> GetAll()
        {
            var client = new RestClient();
            client.BaseUrl = new Uri(serviceUri);

            var request = new RestRequest();
            request.Resource = "courses";

            IRestResponse response = client.Execute(request);

            var objects = JsonConvert.DeserializeObject<List<object>>(response.Content);

            List<Course> courses = new List<Course>();

            foreach (JObject objs in objects)
            {
                courses.Add(ServiceObject.ServiceEntity.TryParse<ServiceObject.Course>(objs.ToString()));
            }
            return courses;
        }

        public ResponseMessage Update(Course entity)
        {
            throw new NotImplementedException();
        }

        public ResponseMessage Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
