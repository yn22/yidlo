﻿using System.Runtime.Serialization;

namespace ServiceLayer.ServiceObject
{
    [DataContract]
    internal class Course : ServiceEntity
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "courseName")]
        public string Name { get; set; }

        [DataMember(Name = "courseCategoryID")]
        public int CategoryID { get; set; }

        [DataMember(Name = "courseContent")]
        public string Content { get; set; }

        [DataMember(Name = "coursePrice")]
        public decimal Price { get; set; }

        [DataMember(Name = "courseIsActive")]
        public bool IsActive { get; set; }

        [DataMember(Name = "courseAvMonday")]
        public bool AvMonday { get; set; }

        [DataMember(Name = "courseAvTuesday")]
        public bool AvTuesday { get; set; }

        [DataMember(Name = "courseAvWednesday")]
        public bool AvWednesday { get; set; }

        [DataMember(Name = "courseAvThursday")]
        public bool AvThursday { get; set; }

        [DataMember(Name = "courseAvFriday")]
        public bool AvFriday { get; set; }

        [DataMember(Name = "courseAvSaturday")]
        public bool AvSaturday { get; set; }

        [DataMember(Name = "courseAvSunday")]
        public bool AvSunday { get; set; }

        public static implicit operator Yidlo.Model.Course(Course course)
        {
            return new Yidlo.Model.Course
            {
                Id = course.ID,
                Name = course.Name,
                Content = course.Content,
                Price = (int)course.Price,  
                Type = GetCategoryName(course.CategoryID),
                isAvailable = course.IsActive,
                avMonday = course.AvMonday,
                avTuesday = course.AvTuesday,
                avWednesday = course.AvWednesday,
                avThursday = course.AvThursday,
                avFriday = course.AvFriday,
                avSaturday = course.AvSaturday,
                avSunday = course.AvSunday
            };
        }

        private static string GetCategoryName(int id)
        {
            switch (id)
            {
                case 1:
                {
                    return "Первое";
                    break;
                }
                case 2:
                {
                    return "Второе";
                    break;
                }
                case 3:
                {
                    return "Салат";
                    break;
                }
                case 4:
                {
                    return "Десерт";
                    break;
                }

                default:
                    return "Invalid course id";
            }
        }
    }
}
