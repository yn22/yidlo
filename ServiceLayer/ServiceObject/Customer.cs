﻿using System.Runtime.Serialization;
using RestSharp.Extensions;

namespace ServiceLayer.ServiceObject
{
    [DataContract]
    internal class Customer : ServiceEntity
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "login")]
        public string Login { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "isActive")]
        public bool IsActive { get; set; }

        public static implicit operator Yidlo.Model.Customer(Customer customer)
        {
            return new Yidlo.Model.Customer()
            {
                ID = customer.ID,
                FirstName = customer.FirstName, 
                LastName = customer.LastName, 
                Login = customer.Login, 
                Password = customer.Password,
                IsActive = customer.IsActive
            };
        }
    }
}