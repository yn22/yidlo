﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ServiceLayer.ServiceObject
{
    internal abstract class ServiceEntity
    {
        internal static T TryParse<T>(string json) where T: ServiceEntity, new()
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
