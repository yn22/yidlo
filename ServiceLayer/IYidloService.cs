﻿using ServiceLayer.ServiceObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayer
{
    public interface IYidloService<T> where T: Yidlo.Model.Entity
    {
        ResponseMessage Add(T entity);
        T Get(int Id);
        T Logining(string Login, string Password);
        List<T> GetAll();
        ResponseMessage Update(T entity);
        ResponseMessage Remove(int id);
    }
}
