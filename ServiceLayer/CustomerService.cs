﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Yidlo.Model;

namespace ServiceLayer
{
    public class CustomerService :IYidloService<Customer>
    {
        private const string serviceUri = "http://10.10.3.69:8080";

        public ResponseMessage Add(Customer customer)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri(serviceUri);

            var request = new RestRequest(Method.POST);
            request.Resource = "customers/add";
            request.AddHeader("Content-type", "application/json");
            string json = JsonConvert.SerializeObject(customer);

            request.AddBody(customer);

            IRestResponse response = client.Execute(request);

            var test = response.Content;
            return new ResponseMessage();
        }

        public Customer Get(int Id)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri(serviceUri);

            var request = new RestRequest();
            request.Resource = "customers/" + Id;

            IRestResponse response = client.Execute(request);

            return ServiceObject.ServiceEntity.TryParse<ServiceObject.Customer>(response.Content);
        }

        public Customer Logining(string Login, string Password)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri(serviceUri);

            var request = new RestRequest();
            request.Resource = "customers/logining" + Login + Password;

            IRestResponse response = client.Execute(request);

            return ServiceObject.ServiceEntity.TryParse<ServiceObject.Customer>(response.Content);

        }

        public List<Customer> GetAll()
        {
            var client = new RestClient();
            client.BaseUrl = new Uri(serviceUri);

            var request = new RestRequest();
            request.Resource = "courses";

            IRestResponse response = client.Execute(request);

            var objects = JsonConvert.DeserializeObject<List<object>>(response.Content);

            List<Customer> courses = new List<Customer>();

            foreach (JObject objs in objects)
            {
                courses.Add(ServiceObject.ServiceEntity.TryParse<ServiceObject.Customer>(objs.ToString()));
            }
            return courses;
        }

        public ResponseMessage Update(Customer entity)
        {
            throw new NotImplementedException();
        }

        public ResponseMessage Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
