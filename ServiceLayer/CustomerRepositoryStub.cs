﻿using System;
using System.Collections.Generic;
using System.Linq;
using Yidlo.Model;
using Yidlo.Model;


namespace ServiceLayer
{
    public class CustomerRepositoryStub : IYidloService<Customer>
    {
        private static readonly object syncObj = new object();

        private static CustomerRepositoryStub _customerService;

        public CustomerRepositoryStub()
        {
            PopulateTestData();
        }

        public static CustomerRepositoryStub CustomerService
        {
            get
            {
                if (_customerService != null) return _customerService;
                lock (syncObj)
                {
                    return _customerService ?? (_customerService = new CustomerRepositoryStub());
                }
            }
        }

        private void PopulateTestData()
        {

            _customer.Add(new Customer(1, "FirstName1", "LastName1", "login1", "password1", true));
            _customer.Add(new Customer(2, "FirstName2", "LastName2", "login2", "password2", false));
            _customer.Add(new Customer(3, "FirstName3", "LastName3", "login3", "password3", true));
        }

        private List<Customer> _customer = new List<Customer>();

        public ResponseMessage Add(Customer customer)
        {
            customer.ID = (_customer.Count > 0 ? _customer.Last().ID + 1 : 0);
            _customer.Add(customer);
            return new ResponseMessage();
        }

        public Customer Get(int ID)
        {
            return _customer.FirstOrDefault(i => i.ID == ID);
        }

        public List<Customer> GetAll()
        {
            return _customer;
        }

        public ResponseMessage Update(Customer entity)
        {
            int matchedID = _customer.FindIndex(x => x.ID == entity.ID);

            if (matchedID == -1)
                throw new ArgumentException("Customer with this id haven't benn found.");

            _customer[matchedID] = entity;

            return new ResponseMessage();
        }

        public ResponseMessage Remove(int id)
        {
            try
            {
                _customer.RemoveAll(x => x.ID == id);
            }
            catch (Exception ex)
            {
                return new ResponseMessage() { Details = ex.Message };
            }

            return new ResponseMessage();
        }

        public Customer Logining(string login, string password)
        {
            return _customer.FirstOrDefault(i => i.Login == login && i.Password == password);
        }

        public int CustomersCount => _customer.Count;
    }
}
