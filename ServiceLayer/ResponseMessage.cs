﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayer
{
    public class ResponseMessage
    {
        public string Message { get; set; }
        public string Details { get; set; }
            
    }
}
