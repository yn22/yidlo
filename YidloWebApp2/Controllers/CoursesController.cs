﻿using System;
using System.Web.Mvc;
using Newtonsoft.Json;
using ServiceLayer;
using Yidlo.Engine;
using Yidlo.Model;

namespace YidloWebApp2.Controllers
{
    public class CoursesController : Controller
    {
        IRepository<Course> _courseRepository;

        public CoursesController()
        {
            _courseRepository = new Yidlo.Engine.CourseRepository(new CourseService());
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Courses()
        {
            return Json(JsonConvert.SerializeObject(_courseRepository.GetAll(), Formatting.Indented),
                JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Course(int id)
        {
            var course = _courseRepository.Find(id);

            if (course == null)
                throw new ArgumentException("Course haven't been found.");

            return Json(JsonConvert.SerializeObject(course, Formatting.Indented), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Remove(int id)
        {
            _courseRepository.Remove(new Course {Id = id});
            return Json("successful", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Add(Course course)
        {
            _courseRepository.Add(course);
            return Json("successful");
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }
    }
}