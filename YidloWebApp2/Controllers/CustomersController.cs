﻿using System;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ServiceLayer;
using Yidlo.Engine;
using Yidlo.Model;
using System.Collections.Generic;

namespace YidloWebApp2.Controllers
{
    public class CustomersController : Controller
    {
        IRepository<Customer> _customerRepository;

        public CustomersController()
        {
            _customerRepository = new Yidlo.Engine.CustomerRepository(new CustomerService());
        }

        [HttpGet]
        public ActionResult Customers()
        {
            return Json(JsonConvert.SerializeObject(_customerRepository.GetAll(), Formatting.Indented),
                JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Customer(int id)
        {
            var customer = _customerRepository.Find(id);

            if (customer == null)
                throw new ArgumentException("Customer haven't been found.");

            return Json(JsonConvert.SerializeObject(customer), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Remove(int id)
        {
            _customerRepository.Remove(new Customer { ID = id });
            return Json("successful", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]

        public ActionResult Save(Customer customer)
        {
            _customerRepository.Add(customer);
            return Json(JsonConvert.SerializeObject(customer), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Logiging(string login, string password)
        {
            var customer = _customerRepository.Logining(login, password);

            if (customer == null)
            {
                return Json("Fail data");
            }
            else
                return Json(JsonConvert.SerializeObject(customer), JsonRequestBehavior.AllowGet);
        }
    }
}