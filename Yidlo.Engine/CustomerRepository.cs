﻿using ServiceLayer;
using Yidlo.Model;
using System;
using System.Collections.Generic;
using Yidlo.Model;

namespace Yidlo.Engine
{
    public class CustomerRepository : IRepository<Customer>
    {
        private IYidloService<Customer> _service;


        public CustomerRepository(IYidloService<Customer> service)
        {
            _service = service;
        }

        public void Remove(Customer entity)
        {
            _service.Remove(entity.ID);
        }

        public void Add(Customer entity)
        {
            if (entity == null)
                throw new ArgumentException("Customer is not specified.");

            _service.Add(entity);
        }

        public void Update(Customer entity)
        {
            _service.Update(entity);
        }

        public List<Customer> GetAll()
        {
            return _service.GetAll();
        }

        public Customer Find(int ID)
        {
            return _service.Get(ID);
        }

        public Customer Logining(string login, string password)
        {
            return _service.Logining(login, password);
        }
    }
}