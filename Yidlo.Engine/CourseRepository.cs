﻿using ServiceLayer;
using Yidlo.Model;
using System;
using System.Collections.Generic;

namespace Yidlo.Engine
{
    public class CourseRepository : IRepository<Course>
    {
        private IYidloService<Course> _service;

        public CourseRepository(IYidloService<Course> service)
        {
            _service = service;
        }

        public Course Logining(string login, string password)
        {
            throw new NotImplementedException();
        }

        public void Remove(Course entity)
        {
            _service.Remove(entity.Id);
        }

        public void Add(Course course)
        {
            if (course == null)
                throw new ArgumentException("Course is not specified.");

           _service.Add(course);
        }

        public void Update(Course entity)
        {
            _service.Update(entity);
        }

        public List<Course> GetAll()
        {
            return _service.GetAll();
        }

        public Course Find(int ID)
        {
            return _service.Get(ID);
        }
    }
}
