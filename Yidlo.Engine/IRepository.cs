﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceLayer.ServiceObject;
using Yidlo.Model;

namespace Yidlo.Engine
{
    public interface IRepository<T> where T : Entity
    {
        List<T> GetAll();
        T Find(int ID);
        T Logining(string login, string password);
        void Remove(T entity);
        void Add(T entity);
        void Update(T entity);
    }
}
