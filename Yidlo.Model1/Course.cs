﻿
namespace Yidlo.Model
{
    public class Course : Entity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public int Price { get; set; }
        public string Type { get; set; }
        public bool isAvailable { get; set; }
        public bool avMonday { get; set; }
        public bool avTuesday { get; set; }
        public bool avWednesday { get; set; }
        public bool avThursday { get; set; }
        public bool avFriday { get; set; }
        public bool avSaturday { get; set; }
        public bool avSunday { get; set; }

        public Course(int _id, string _name, string _content, int _price, string _type, bool[] _availability)
        {
            Id = _id;
            Name = _name;
            Content = _content;
            Price = _price;
            Type = _type;
            avMonday = _availability[0];
            avTuesday = _availability[1];
            avWednesday = _availability[2];
            avThursday = _availability[3];
            avFriday = _availability[4];
            avSaturday = _availability[5];
            avSunday = _availability[6];
            isAvailable = true;
        }

        public Course()
        {
        }
    }
}
