﻿using System.Runtime.Serialization;

namespace Yidlo.Model
{
    [DataContract]
    public class Customer : Entity
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }
        [DataMember(Name = "firstName")]
        public string FirstName;
        [DataMember(Name = "lastName")]
        public string LastName;
        [DataMember(Name = "login")]
        public string Login;
        [DataMember(Name = "password")]
        public string Password;
        [DataMember(Name = "isActive")]
        public bool IsActive;

        public Customer(int customerID, string customerFirstName, string customerLastName, string customerLogin, string customerPassword, bool customerIsActive)
        {
            ID = customerID;
            FirstName = customerFirstName;
            LastName = customerLastName;
            Login = customerLogin;
            Password = customerPassword;
            IsActive = customerIsActive;
        }

        public Customer() { }
    }
}
