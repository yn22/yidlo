﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yidlo.Model
{
    public class Course : Entity
    {

        public int id { get; set; }
        public string name { get; set; }
        public string content { get; set; }
        public int price { get; set; }
        public string type { get; set; }
        public bool isAvailable { get; set; }
        public bool avMonday { get; set; }
        public bool avTuesday { get; set; }
        public bool avWednesday { get; set; }
        public bool avThursday { get; set; }
        public bool avFriday { get; set; }
        public bool avSaturday { get; set; }
        public bool avSunday { get; set; }

        public Course(int _id, string _name, string _content, int _price, string _type, bool[] _availability)
        {
            id = _id;
            name = _name;
            content = _content;
            price = _price;
            type = _type;
            avMonday = _availability[0];
            avTuesday = _availability[1];
            avWednesday = _availability[2];
            avThursday = _availability[3];
            avFriday = _availability[4];
            avSaturday = _availability[5];
            avSunday = _availability[6];
            isAvailable = true;
        }

        public Course()
        { }
    }
}
